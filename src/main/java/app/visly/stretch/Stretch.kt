package app.visly.stretch

import java.io.File
import java.lang.ref.WeakReference
import java.nio.file.Files

class Stretch {
    internal var ptr: Long

    init {
        prepareBinary()
        ptr = nInit()
    }

    internal val nodes = HashSet<Node>()

    fun free() {
        nodes.toList().forEach(Node::free)
        nFree(ptr)
    }

    companion object {
        private var didInit = false
        fun prepareBinary() {
            if (didInit) return
            synchronized(didInit) {
                if (!didInit) {
                    Files.createTempDirectory("stretch-lib").toFile().let { folder ->
                        folder.deleteOnExit()

                        val osName = System.getProperty("os.name").toLowerCase()
                        val libFileName = if (osName.contains("win")) "stretch.dll" else "libstretch.so"
                        val libFilePath = when {
                            osName.contains("win") -> "/libs/x86_64-pc-windows/stretch.dll"
                            osName.contains("mac") -> "/libs/x86_64-apple-darwin/libstretch.so"
                            osName.contains("nix") || osName.contains("nux") || osName.contains("aix") ->
                                "/libs/x86_64-unknown-linux/libstretch.so"
                            else -> throw TODO("No stretch lib for the platform: $osName")
                        }
                        val libFileURL = this::class.java.getResource(libFilePath)
                            ?: throw IllegalStateException("Missing lib file: $libFilePath")

                        File(folder, libFileName).let { libFile ->
                            libFile.deleteOnExit()

                            libFileURL.openStream().use { input ->
                                Files.copy(input, libFile.toPath())
                            }

                            System.load(libFile.absolutePath)
                        }
                    }

                    didInit = true
                }
            }
        }
    }

    private external fun nInit(): Long
    private external fun nFree(stretch: Long)
}
