package app.visly.stretch

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class StretchTest {
    @Test
    fun stretch_createAndFree() {
        Stretch().also { it.free() }
        assertTrue(true)
    }

    @Test
    fun stretch_createNodeWithStyle() {
        Stretch().also { stretch ->
            val node = Node(stretch, Style(alignSelf = AlignSelf.FlexEnd), listOf())
            assertEquals(node.getStyle().alignSelf, AlignSelf.FlexEnd)
        }.free()
    }

    @Test
    fun stretch_createNodeWithStyleBulk() {
        repeat(1000) {
            Stretch().also { stretch ->
                repeat(1000) {
                    val node = Node(stretch, Style(alignSelf = AlignSelf.FlexEnd), listOf())
                    assertEquals(node.getStyle().alignSelf, AlignSelf.FlexEnd)
                }
            }.free()
        }
    }
}
