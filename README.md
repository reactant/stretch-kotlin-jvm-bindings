# Stretch kotlin bindings for jvm
Fork from stretch kotlin android binding: https://github.com/vislyhq/stretch

Use for access stretch from Kotlin jvm environment
## Compile
```bash
# Compile stretch for target platform
gradle rust-sync-x86_64-unknown-linux
gradle rust-sync-x86_64-pc-windows
gradle rust-sync-x86_64-apple-darwin

gradle build
```
